import express, { Router } from 'express';
import PostController from '../controllers/postController';

const router: Router = express.Router();
const postController = new PostController();

// Route to create a new post
router.post('/create-post', postController.createPost);

// Route to like a post
router.post('/like', postController.likePost);

// Route to delete a post
router.delete('/:postId', postController.deletePost);

// Route to update a post
router.put('/:postId', postController.updatePost);

// Route to fetch posts from users followed by the current user
router.get('/feed/:userId', postController.getFeed);

// Route to get the number of likes for a post
router.get('/likes/:postId', postController.getLikesCount);

// Route to get the number of comments for a post
router.get('/comments/:postId', postController.getCommentsCount);

export default router;
