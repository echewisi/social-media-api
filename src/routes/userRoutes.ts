import express, { Router } from "express";
import UserController from "../controllers/userController";

const router: Router = express.Router();
const userController = new UserController();

// Route to register a new user
router.post("/register", userController.registerUser);

// Route to log in a user
router.post("/login", userController.loginUser);

// Route to update user information
router.put("/:userId", userController.updateUser);

export default router;
