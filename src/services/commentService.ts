import { CommentModel } from "../models/Comment";
import { UserModel } from "../models/User";
import { PostModel } from "../models/Post";
import NotificationService from "./notificationService";

class CommentService {
  private notificationService: NotificationService; // Add notificationService property

  constructor(notificationService: NotificationService) {
    this.notificationService = notificationService;
  }
  // Method to create a new comment
  async createComment(
    authorId: string,
    postId: string,
    content: string
  ): Promise<any> {
    try {
      // Check if the author exists
      const author = await UserModel.findById(authorId);
      if (!author) {
        throw new Error("User not found");
      }

      // Check if the post exists
      const post = await PostModel.findById(postId);
      if (!post) {
        throw new Error("Post not found");
      }

      const newComment = new CommentModel({
        author: authorId,
        post: postId,
        content,
      });
      await newComment.save();

      // Send notification to the post owner
      const postOwner = await UserModel.findById(post.author);
      if (postOwner) {
        const notificationMessage = `User ${author.username} commented on your post: "${content}"`;
        await this.notificationService.createNotification(
          postOwner._id,
          "comment",
          notificationMessage
        );
      }

      return newComment;
    } catch (error) {
      throw error;
    }
  }

  // Method to delete a comment
  async deleteComment(commentId: string): Promise<void> {
    try {
      const deletedComment = await CommentModel.findByIdAndDelete(commentId);
      if (!deletedComment) {
        throw new Error("Comment not found");
      }
    } catch (error) {
      throw error;
    }
  }

  // Method to update a comment
  async updateComment(
    commentId: string,
    updates: Partial<typeof CommentModel>
  ): Promise<any> {
    try {
      const updatedComment = await CommentModel.findByIdAndUpdate(
        commentId,
        updates,
        { new: true }
      );
      if (!updatedComment) {
        throw new Error("Comment not found");
      }
      return updatedComment;
    } catch (error) {
      throw error;
    }
  }

  // Method to get comments for a post
  async getCommentsForPost(postId: string): Promise<any> {
    try {
      const comments = await CommentModel.find({ post: postId });
      return comments;
    } catch (error) {
      throw error;
    }
  }
}

export default CommentService;
