import { Request, Response } from "express";
import PostService from "../services/postService";

class PostController {
  private postService: PostService; // Define a private instance variable for PostService

  constructor(postService: PostService) {
    // Initialize PostService in the constructor
    this.postService = postService;
  }
  // Method to create a new post
  public async createPost(req: Request, res: Response): Promise<void> {
    try {
      const { userId, content, attachments } = req.body; // Extract userId, content, and attachments from the request body
      const newPost = await this.postService.createPost(
        userId,
        content,
        attachments
      );
      res.status(201).json(newPost);
    } catch (error) {
      res.status(500).json({ message: "Internal server error" });
    }
  }

  // Method to like a post
  public async likePost(req: Request, res: Response): Promise<void> {
    try {
      const { postId, userId } = req.body; // Extract postId and userId from the request body
      const likedPost = await this.postService.likePost(postId, userId);
      res.status(200).json(likedPost);
    } catch (error) {
      res.status(500).json({ message: "Internal server error" });
    }
  }

  // Method to delete a post
  public async deletePost(req: Request, res: Response): Promise<void> {
    try {
      const { postId } = req.params; // Extract postId from the request parameters
      await this.postService.deletePost(postId);
      res.status(204).end();
    } catch (error) {
      res.status(500).json({ message: "Internal server error" });
    }
  }

  // Method to update a post
  public async updatePost(req: Request, res: Response): Promise<void> {
    try {
      const { postId } = req.params; // Extract postId from the request parameters
      const updates = req.body; // Get updates from the request body
      const updatedPost = await this.postService.updatePost(postId, updates);
      res.status(200).json(updatedPost);
    } catch (error) {
      res.status(500).json({ message: "Internal server error" });
    }
  }

  // Method to fetch posts from users followed by the current user
  public async getFeed(req: Request, res: Response): Promise<void> {
    try {
      const { userId } = req.params; // Extract userId from the request parameters
      const page = Number(req.query.page); // Parse pagination parameters from the query string
      const limit = Number(req.query.limit);
      const posts = await this.postService.getFeed(userId, page, limit); // Pass page and limit as numbers
      res.status(200).json(posts);
    } catch (error) {
      res.status(500).json({ message: "Internal server error" });
    }
  }

  // Method to get the number of likes for a post
  public async getLikesCount(req: Request, res: Response): Promise<void> {
    try {
      const { postId } = req.params; // Extract postId from the request parameters
      const likesCount = await this.postService.getLikesCount(postId);
      res.status(200).json({ likesCount });
    } catch (error) {
      res.status(500).json({ message: "Internal server error" });
    }
  }

  // Method to get the number of comments for a post
  public async getCommentsCount(req: Request, res: Response): Promise<void> {
    try {
      const { postId } = req.params; // Extract postId from the request parameters
      const commentsCount = await this.postService.getCommentsCount(postId);
      res.status(200).json({ commentsCount });
    } catch (error) {
      res.status(500).json({ message: "Internal server error" });
    }
  }
}

export default  PostController;
