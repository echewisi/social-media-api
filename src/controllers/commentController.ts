import { Request, Response } from 'express';
import CommentService from '../services/commentService';

class CommentController {
  private commentService: CommentService;

  constructor(commentService: CommentService) {
    this.commentService = commentService;
  }

  // Method to create a new comment
  public async createComment(req: Request, res: Response): Promise<void> {
    try {
      const { authorId, postId, content } = req.body; // Assuming authorId, postId, and content are extracted from the request body
      const newComment = await this.commentService.createComment(authorId, postId, content);
      res.status(201).json(newComment);
    } catch (error) {
      res.status(500).json({ message: 'Internal server error' });
    }
  }

  // Method to delete a comment
  public async deleteComment(req: Request, res: Response): Promise<void> {
    try {
      const { commentId } = req.params; // Assuming commentId is extracted from the request parameters
      await this.commentService.deleteComment(commentId);
      res.status(204).end();
    } catch (error) {
      res.status(500).json({ message: 'Internal server error' });
    }
  }

  // Method to update a comment
  public async updateComment(req: Request, res: Response): Promise<void> {
    try {
      const { commentId } = req.params; // Assuming commentId is extracted from the request parameters
      const updates = req.body; // Assuming updates are provided in the request body
      const updatedComment = await this.commentService.updateComment(commentId, updates);
      res.status(200).json(updatedComment);
    } catch (error) {
      res.status(500).json({ message: 'Internal server error' });
    }
  }

  // Method to get comments for a post
  public async getCommentsForPost(req: Request, res: Response): Promise<void> {
    try {
      const { postId } = req.params; // Assuming postId is extracted from the request parameters
      const comments = await this.commentService.getCommentsForPost(postId);
      res.status(200).json(comments);
    } catch (error) {
      res.status(500).json({ message: 'Internal server error' });
    }
  }
}

export default CommentController;
